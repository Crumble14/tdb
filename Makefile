NAME = TDb
COMPILER = gcc
FLAGS = -Wall -Wextra -Werror -O3 -std=c++17 -pthread -g

SRC_DIR = src/
SRC := $(shell find $(SRC_DIR) -type f -name "*.cpp")

DIRS := $(shell find $(SRC_DIR) -type d)

OBJ_DIR = obj/
OBJ_DIRS := $(patsubst $(SRC_DIR)%, $(OBJ_DIR)%, $(DIRS))
OBJ := $(patsubst $(SRC_DIR)%.cpp, $(OBJ_DIR)%.o, $(SRC))

LIBS = -I ../JSONParser/src -I ../TEngine/src -I ../Tungsten/src -I ../SockLib/src -lstdc++ -lm -ldl

.PHONY: all $(NAME) mkdir clean fclean re
.SILENT: all $(NAME) mkdir clean fclean re

all: $(NAME)

$(NAME): mkdir $(OBJ)
	echo "Linking..."
	$(COMPILER) $(FLAGS) -o $(NAME) $(OBJ) *.a $(LIBS)

mkdir:
	echo "Making object directories..."
	mkdir -p $(OBJ_DIRS)

$(OBJ_DIR)%.o: $(SRC_DIR)%.cpp
	@echo "Compiling:" $<
	@$(COMPILER) $(FLAGS) -c $< -o $@ $(LIBS)

clean:
	echo "Cleaning objects..."
	rm -rf $(OBJ_DIR)

fclean: clean
	echo "Removing binary..."
	rm -f $(NAME)

re: fclean all
