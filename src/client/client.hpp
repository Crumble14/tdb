#ifndef CLIENT_HPP
# define CLIENT_HPP

# include<memory>
# include<thread>

# include<tengine.hpp>
# include<socket.hpp>

# define BUFFER_SIZE	1048576

namespace TDb
{
	using namespace std;
	using namespace Network;

	class Database;
	class User;
	class error_exception;

	struct Buffer
	{
		char* data;
		size_t size = 0;
		size_t i = 0;

		inline Buffer()
		{
			if(!(data = (char*) malloc(BUFFER_SIZE))) throw bad_alloc();
		}

		inline Buffer(const Buffer& buffer)
		{
			if(!(data = (char*) malloc(BUFFER_SIZE))) throw bad_alloc();
			memcpy(data, buffer.data, buffer.size);
		}

		inline ~Buffer()
		{
			free((void*) data);
		}

		void read(const sock_t sock);

		inline size_t available() const
		{
			return size - i;
		}

		template<typename T>
		inline bool get(T& buff)
		{
			if(available() >= sizeof(buff)) {
				memcpy(&buff, data + i, sizeof(buff));
				i += sizeof(buff);

				return true;
			}

			i = 0;
			return false;
		}

		bool get(string& buff, const size_t length);
		bool get_string(string& buff);

		inline void reset()
		{
			size = 0;
			i = 0;
		}
	};

	class Client
	{
		public:
			Client(Database& database, const sock_t sock);
			~Client();

			inline Database& get_database()
			{
				return *database;
			}

			inline sock_t get_socket()
			{
				return sock;
			}

			inline bool is_logged() const
			{
				return (user != nullptr);
			}

			inline const User* get_user() const
			{
				return user;
			}

			inline bool is_ready() const
			{
				return sock_is_ready(sock.fd);
			}

			void handle();

			void send_error(const error_exception& error);

		private:
			Database* database;
			sock_t sock;

			Buffer buff;

			User* user = nullptr;

			template<typename T>
			inline void write_value(const T&& value)
			{
				sock_write(sock.fd, (const char*)&value, sizeof(T));
			}

			void handle_login();
			void handle_get();
			void handle_set();
			void handle_insert();
			void handle_remove();
			void handle_clear();
			void handle_process();

			bool get_table(Table*& table);

			bool login(const string& user, const string& pass);

			void send_data_head(const vector<Column>& columns);
			void send_data_end(const size_t size);
	};

	class ClientsManager
	{
		public:
			ClientsManager() = default;

			inline ClientsManager(Database& database, const port_t port)
				: database{&database}
			{
				init(port);
			}

			inline Database& get_database()
			{
				return *database;
			}

			inline vector<Client>& get_clients()
			{
				return clients;
			}

		private:
			Database* database;

			int sock;
			vector<Client> clients;

			void init(const port_t port);
	};
}

#endif
