#include "client.hpp"
#include "../tdb.hpp"

using namespace TDb;

void ClientsManager::init(const port_t port)
{
	sock = sock_create(false);
	sock_listen(sock, port);

	thread listen_thread([this]()
		{
			try {
				while(true) {
					const auto s = sock_accept(sock);
					clients.emplace_back(*database, s);
				}
			} catch(const socket_exception&) {}
		});

	thread clients_thread([this]()
		{
			while(true) {
				auto c = clients.begin();

				while(c != clients.end()) {
					try {
						if(c->is_ready()) c->handle();
						++c;
					} catch(const error_exception& e) {
						c->send_error(e);
					} catch(const socket_exception&) {
						clients.erase(c);
					}
				}

				this_thread::sleep_for(1ms);
			}
		});

	listen_thread.detach();
	clients_thread.detach();
}
