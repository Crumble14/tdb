#include "tdb.hpp"

using namespace TDb;

string TDb::get_date()
{
	const auto now = time(0);
	const auto ts = *localtime(&now);
	array<char, 80> buff;

	strftime(buff.data(), buff.size(), "%Y-%m-%d", &ts);
	return string(buff.data());
}

string TDb::get_log_time()
{
	const auto now = time(0);
	const auto ts = *localtime(&now);
	array<char, 80> buff;

	strftime(buff.data(), buff.size(), "%a %Y/%m/%d %H:%M:%S %Z", &ts);
	return string(buff.data());
}

bool TDb::make_dir(const char* path)
{
	mkdir(path, 0777);

	if(errno == EEXIST) {
		errno = 0;
		return false;
	}

	errno = 0;
	return true;
}

void TDb::foreach_file(const char* dir_path,
	const function<void(const string&)> handle)
{
	DIR* dir;
	dirent* entry;

	if(!(dir = opendir(dir_path))) {
		throw runtime_error("Failed to open directory!");
	}

	while((entry = readdir(dir))) {
		const string name(entry->d_name);
		if(name == "." || name == "..") continue;

		handle(name);
	}

	closedir(dir);
}

string TDb::random_string(const size_t length)
{
	string str;
	str.reserve(length);

	const char min = 33;
	const char max = 126;

	for(size_t i = 0; i < length; ++i) {
		// TODO Debug
		str[i] = min + (rand() * (max - min) / RAND_MAX);
	}

	return str;
}

string TDb::hash(const string input)
{
	// TODO
	return input;
}

vector<string> TDb::str_split(const string& str, const char sp)
{
	vector<string> result;
	stringstream ss(str);
	string buffer;

	while(getline(ss, buffer, sp)) {
		result.push_back(buffer);
	}

	return result;
}

const char* TDb::get_error_str(const Error error)
{
	return errors.find(error)->second;
}
