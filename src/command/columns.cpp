#include "command.hpp"
#include "../tdb.hpp"

using namespace TDb;

void c_list(const Table& table)
{
	const auto& columns = table.get_columns();
	cout << "Columns list for table `" << table.get_name()
		<< "` (" << columns.size() << " elements):" << '\n';

	for(const auto& c : columns) {
		cout << "- " << c.get_name() << " (Type: " << get_type(c.get_type())
			<< ", Size: " << ((unsigned int) c.get_size()) << ")" << '\n';
	}
}

void c_set(Table& table, const vector<string>& args)
{
	if(args.size() < 4 && args.size() > 5) {
		invalid_usage();
		return;
	}

	const auto& name = args[2];
	const auto& type = get_type(args[3]);

	const auto& size = (args.size() == 5 ?
		stoi(args[4]) : get_default_size(type));
	// TODO Check size limits

	Column column(name, type, size);
	table.set_column(column);

	cout << "Column `" << name << "` set!" << '\n';
}

void c_rename(Table& table, const vector<string>& args)
{
	if(args.size() != 4) {
		invalid_usage();
		return;
	}

	const auto& column = args[2];
	const auto& new_name = args[3];

	(void) table;
	// TODO

	cout << "Column `" << column << "` renamed into `" << new_name << "`!" << '\n';
}

void c_remove(Table& table, const vector<string>& args)
{
	if(args.size() != 3) {
		invalid_usage();
		return;
	}

	const auto& column = args[2];

	if(!table.get_column(column)) {
		cout << "Column `" << column << "` doesn't exist!" << '\n';
		return;
	}

	table.remove_column(column);

	cout << "Column `" << column << "` removed!" << '\n';
}

void ColumnsCommand::perform(Database& database,
	const vector<string>& args) const
{
	if(args.empty()) {
		invalid_usage();
		return;
	}

	auto table = database.get_table(args.front());

	if(!table) {
		cout << "Table `" << args.front() << "` not found!" << '\n';
		return;
	}

	if(args.size() == 1) {
		c_list(*table);
		return;
	}

	const auto& action = args[1];

	if(action == "list") {
		c_list(*table);
	} else if(action == "set") {
		c_set(*table, args);
	} else if(action == "rename") {
		c_rename(*table, args);
	} else if(action == "remove") {
		c_remove(*table, args);
	} else {
		invalid_usage();
	}
}
