#include "command.hpp"
#include "../tdb.hpp"

using namespace TDb;

void ExitCommand::perform(Database& database, const vector<string>&) const
{
	database.log("Exiting...");
	exit(0);
}
