#include "command.hpp"
#include "../tdb.hpp"

using namespace TDb;

void u_list(Database& database)
{
	const auto& users = database.get_users();
	cout << "Users list (" << users.size() << " elements):" << '\n';

	for(const auto& u : users) {
		cout << u.get_name() << '\n';
	}
}

void u_create(Database& database, const vector<string>& args)
{
	if(args.size() != 3) {
		invalid_usage();
		return;
	}

	const auto& name = args[1];
	cout << "User `" << name;

	if(database.create_user(name, args[2])) {
		cout << "` created!";
	} else {
		cout << "` already exists!";
	}

	cout << '\n';
}

void u_rename(Database& database, const vector<string>& args)
{
	if(args.size() != 3) {
		invalid_usage();
		return;
	}

	const auto user = database.get_user(args[1]);

	if(database.get_user(args[2])) {
		cout << "Name `" << args[2] << "` already used!" << '\n';
		return;
	}

	cout << "User `" << args[1];

	if(user) {
		user->remove_user();
		user->set_name(args[2]);
		user->save();

		cout << "` renamed into `" << args[2] << "`!";
	} else {
		cout << "` not found!";
	}

	cout << '\n';
}

void u_setpassword(Database& database, const vector<string>& args)
{
	if(args.size() != 3) {
		invalid_usage();
		return;
	}

	const auto user = database.get_user(args[1]);
	cout << "User `" << args[1];

	if(user) {
		user->set_password(args[2], false);
		user->save();

		cout << "`'s password changed!";
	} else {
		cout << "` not found!";
	}

	cout << '\n';
}

void u_remove(Database& database, const vector<string>& args)
{
	if(args.size() != 2) {
		invalid_usage();
		return;
	}

	const auto& name = args[1];
	cout << "User `" << name;

	if(database.remove_user(name)) {
		cout << "` removed!";
	} else {
		cout << "` not found!";
	}

	cout << '\n';
}

void UsersCommand::perform(Database& database, const vector<string>& args) const
{
	if(args.empty()) {
		u_list(database);
		return;
	}

	const auto& action = args.front();

	if(action == "list") {
		u_list(database);
	} else if(action == "create") {
		u_create(database, args);
	} else if(action == "rename") {
		u_rename(database, args);
	} else if(action == "setpassword") {
		u_setpassword(database, args);
	} else if(action == "remove") {
		u_remove(database, args);
	} else {
		invalid_usage();
	}
}
