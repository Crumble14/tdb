#include "process.hpp"

using namespace TDb;

static inline bool is_space(const char c)
{
	return (c == ' ' || c == '\t' || c == '\n');
}

static inline void skip_spaces(const char*& str)
{
	while(*str && is_space(*str)) {
		++str;
	}
}

/* static string get_word(const char*& str)
{
	skip_spaces(str);

	string word;

	while(*str && !is_space(*str)) {
		word += *str;
		++str;
	}

	return word;
} */

Program TDb::compile(const string& str)
{
	(void) str;
	// TODO
	return Program();
}
